import sys
import os
# sample(self, list, int)
from random import randint, sample

class Room(object):
	
	def enter(self):
		pass

#########
##ROOMS############
#########		
class FrontYard(Room):
	
	def enter(self):
		weps = []	
		weaponslist = []
		weapons1 = open('weaponlist.txt', 'r')
		for line in weapons1:
			weaponslist.append(line)
		weapons1.close()
		choosewep = sample(weaponslist, 3)
		for weapon in choosewep:
			weapon = weapon.split()
			weapon = ''.join(weapon)
			weps.append(weapon)
		
		print "WELCOME!"
		print "\nYou are 12 years old. You are looking for your caregiver."
		print "\nHer name is Gloria."
		print "\nYou accidentally slipped bath salts into her food."
		print "\nShe is lost, indeed."
		print "\nIf found, you must defend yourself."
		print "\nYou will start your search from the FrontYard."
		print "\nYou walk up to the front porch, and find a collection of weapons."
		print "\nChoose wisely!"
		for weapon in weps:
			print "Do you want a %s?" % weapon
		##### loop for weapon choice
		x = 0
		while x < 1:
			choice = raw_input("What is your choice of blunder?---> ")
			if choice in weps:
				game1.inventory.append(choice)
				print "%s added to your inventory! %s equipped!" % (choice,choice)
				x += 1
			else:
				print "I can't understand that!"
		#####
		print "\nWhich way now?"
		answer = raw_input("CentralHallway?--->")
		if answer.lower() == "centralhallway":
			game1.room = 'CentralHallway'
			return
		
class CentralHallway(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("DiningRoom or Stairs?---> ")
		if answer.lower() == "diningroom":
			game1.room = 'Dining'
			return
		elif answer.lower() == "stairs":
			game1.room = 'Stairs'
			return
	
class Stairs(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("CentralHallway, DownStairs, or Library?---> ")
		if answer.lower() == "centralhallway":
			game1.room = 'CentralHallway'
			return
		elif answer.lower() == "downstairs":
			game1.room = 'Basement'
			return
		elif answer.lower() == "library":
			game1.room = 'Library'
			return
	
class Basement(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("UpStairs?---> ")
		if answer.lower() == "upstairs":
			game1.room = 'Stairs'
			return
	
class Library(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("Stairs or Kitchen?---> ")
		if answer.lower() == "stairs":
			game1.room = 'Stairs'
			return
		elif answer.lower() == "kitchen":
			game1.room = 'Kitchen'
			return
	
class Kitchen(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("Library or DiningRoom?---> ")
		if answer.lower() == "library":
			game1.room = 'Library'
			return
		elif answer.lower() == "diningroom":
			game1.room = 'Dining'
			return
	
class Dining(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("Kitchen, CentralHallway, or Den?---> ")
		if answer.lower() == "kitchen":
			game1.room = 'Kitchen'
			return
		elif answer.lower() == "centralhallway":
			game1.room = 'CentralHallway'
			return
		elif answer.lower() == "den":
			game1.room = 'Den'
			return
	
class Den(Room):
	
	def enter(self):
		game1.potion()
		game1.gloria()
		if game1.room == 'Death':
			return
		game1.phrase()
		print "\nWhich way now?"
		answer = raw_input("DiningRoom?---> ")
		if answer.lower() == "diningroom":
			game1.room = 'Dining'
			return		
########
##END##ROOMS########
########



########
##ENGINE############
########
class Engine(object):
	rooms = {
			'FrontYard':FrontYard(),
			'CentralHallway':CentralHallway(),
			'Stairs':Stairs(),
			'Basement':Basement(),
			'Library':Library(),
			'Kitchen':Kitchen(),
			'Dining':Dining(),
			'Den':Den()
			}
	powerlist = [
				"Time Control",
				"Telekenesis",
				"Teleportation",
				"Omnipotence",
				"Invisibility",
				"Super Speed",
				"Super Strength",
				"Shapeshifting",
				"Earthbending",
				"Waterbending",
				"Airbending",
				"Firebending"
				]
	powerlist1 = [
				"Time Control",
				"Telekenesis",
				"Teleportation",
				"Omnipotence",
				"Invisibility",
				"Super Speed",
				"Super Strength",
				"Shapeshifting",
				"Earthbending",
				"Waterbending",
				"Airbending",
				"Firebending"
				]
	eyesphrases = [
					"You feel eyes on you from somewhere in the room...",
					"You hear the ticking clock...",
					"You see blood dripping from the ceiling...",
					"You hear a sharp 'EEEEEEEEEEE!' from another room...",
					"You feel a psychic force nearby....",
					"You smell... bleach..." 
					]
	powers = []
	inventory = []
	dead = ''
	
	def __init__(self, room):
		self.room = room
		
	def phrase(self):
		j = sample(self.eyesphrases,1)
		for i in j:
			print "\n",i
		return
	
	def potion(self):
		potion = randint (1,4)
		findpotion = randint(1,4)
		if potion == findpotion:
			spower = sample(self.powerlist, 1)
			print "\nYou found a magic potion in this room!"
			print "\nYou drink the potion!"
			raw_input("\nYou feel very strange...[ENTER]")
			print "\nYou have gained a new superpower!"
			for i in spower:
				self.powers.append(i)
				self.powerlist.remove(i)
				print i, " added to powers!"
		return
		
	
	def gloria(self):
		deathnum = randint(1,3)
		gloria = randint(1,3)
		# debug
		#print "D#",deathnum,"G#",gloria
		# IF GLORIA
		if deathnum == gloria:
			for power in self.powers:
				if power in self.powerlist1:
					samp1 = sample(self.inventory,1)
					samp2 =	sample(self.powers,1)
					print "\nGloria attacked!"
					print "\nYou defeated her with your %s and your %s power!" % (samp1[0],samp2[0])
					print "Congratulation!"
					self.dead = 'YOU WIN!'
					self.room = 'Death'
					return 
			print "\nGloria attacked you from behind!"	
			self.dead = "\nGloria evaded your counter with the %s, and ate your heart." % self.inventory[0]
			self.room = 'Death'
			return
			
		
			
		
	def play(self):
		while True and self.room in self.rooms:
			deathnum = randint(1,6)
			gloria = randint(1,5)
			a = self.rooms[self.room].enter()
			a
		if self.room == 'Death':
			self.death()
		else:
			exit(1)
	
	def death(self):
		print self.dead
		print "\nPlayAgain?"
		answer = raw_input("\ny/n?----> ")
		if answer.lower() == "y" or answer.lower() == "yes":
			self.restart_program()
		else:
			exit(1)
			
	def restart_program(self):
		"""Restarts the current program.
		Note: this function does not return. Any cleanup action (like
		saving data) must be done before calling this function."""
		python = sys.executable
		os.execl(python, python, * sys.argv)
	
			
########
##END##ENGINE#######
########
game1 = Engine('FrontYard')
game1.play()
